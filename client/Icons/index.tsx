export const TagIcon = () => {
  return (
    <>
      <svg
        width="16"
        height="12"
        viewBox="0 0 16 12"
        fill="none"
        xmlns="http://www.w3.org/2000/svg"
      >
        <path
          d="M5.8775 1.5L7.3775 3H14V10.5H2V1.5H5.8775ZM6.5 0H2C1.175 0 0.5075 0.675 0.5075 1.5L0.5 10.5C0.5 11.325 1.175 12 2 12H14C14.825 12 15.5 11.325 15.5 10.5V3C15.5 2.175 14.825 1.5 14 1.5H8L6.5 0Z"
          fill="#808191"
        />
      </svg>
    </>
  );
};
export const Loader = () => {
  return (
    <>
      <svg
        xmlns="http://www.w3.org/2000/svg"
        xmlnsXlink="http://www.w3.org/1999/xlink"
        style={{ margin: "auto", background: "transparent", display: "block" }}
        width="200px"
        height="200px"
        viewBox="0 0 100 100"
        preserveAspectRatio="xMidYMid"
        // className="m-auto bg-transparent "
      >
        <g transform="rotate(0 50 50)">
          <rect
            x="47"
            y="24"
            rx="2.52"
            ry="2.52"
            width="6"
            height="12"
            fill="#1dc071"
          >
            <animate
              attributeName="opacity"
              values="1;0"
              keyTimes="0;1"
              dur="1s"
              begin="-0.9166666666666666s"
              repeatCount="indefinite"
            ></animate>
          </rect>
        </g>
        <g transform="rotate(30 50 50)">
          <rect
            x="47"
            y="24"
            rx="2.52"
            ry="2.52"
            width="6"
            height="12"
            fill="#1dc071"
          >
            <animate
              attributeName="opacity"
              values="1;0"
              keyTimes="0;1"
              dur="1s"
              begin="-0.8333333333333334s"
              repeatCount="indefinite"
            ></animate>
          </rect>
        </g>
        <g transform="rotate(60 50 50)">
          <rect
            x="47"
            y="24"
            rx="2.52"
            ry="2.52"
            width="6"
            height="12"
            fill="#1dc071"
          >
            <animate
              attributeName="opacity"
              values="1;0"
              keyTimes="0;1"
              dur="1s"
              begin="-0.75s"
              repeatCount="indefinite"
            ></animate>
          </rect>
        </g>
        <g transform="rotate(90 50 50)">
          <rect
            x="47"
            y="24"
            rx="2.52"
            ry="2.52"
            width="6"
            height="12"
            fill="#1dc071"
          >
            <animate
              attributeName="opacity"
              values="1;0"
              keyTimes="0;1"
              dur="1s"
              begin="-0.6666666666666666s"
              repeatCount="indefinite"
            ></animate>
          </rect>
        </g>
        <g transform="rotate(120 50 50)">
          <rect
            x="47"
            y="24"
            rx="2.52"
            ry="2.52"
            width="6"
            height="12"
            fill="#1dc071"
          >
            <animate
              attributeName="opacity"
              values="1;0"
              keyTimes="0;1"
              dur="1s"
              begin="-0.5833333333333334s"
              repeatCount="indefinite"
            ></animate>
          </rect>
        </g>
        <g transform="rotate(150 50 50)">
          <rect
            x="47"
            y="24"
            rx="2.52"
            ry="2.52"
            width="6"
            height="12"
            fill="#1dc071"
          >
            <animate
              attributeName="opacity"
              values="1;0"
              keyTimes="0;1"
              dur="1s"
              begin="-0.5s"
              repeatCount="indefinite"
            ></animate>
          </rect>
        </g>
        <g transform="rotate(180 50 50)">
          <rect
            x="47"
            y="24"
            rx="2.52"
            ry="2.52"
            width="6"
            height="12"
            fill="#1dc071"
          >
            <animate
              attributeName="opacity"
              values="1;0"
              keyTimes="0;1"
              dur="1s"
              begin="-0.4166666666666667s"
              repeatCount="indefinite"
            ></animate>
          </rect>
        </g>
        <g transform="rotate(210 50 50)">
          <rect
            x="47"
            y="24"
            rx="2.52"
            ry="2.52"
            width="6"
            height="12"
            fill="#1dc071"
          >
            <animate
              attributeName="opacity"
              values="1;0"
              keyTimes="0;1"
              dur="1s"
              begin="-0.3333333333333333s"
              repeatCount="indefinite"
            ></animate>
          </rect>
        </g>
        <g transform="rotate(240 50 50)">
          <rect
            x="47"
            y="24"
            rx="2.52"
            ry="2.52"
            width="6"
            height="12"
            fill="#1dc071"
          >
            <animate
              attributeName="opacity"
              values="1;0"
              keyTimes="0;1"
              dur="1s"
              begin="-0.25s"
              repeatCount="indefinite"
            ></animate>
          </rect>
        </g>
        <g transform="rotate(270 50 50)">
          <rect
            x="47"
            y="24"
            rx="2.52"
            ry="2.52"
            width="6"
            height="12"
            fill="#1dc071"
          >
            <animate
              attributeName="opacity"
              values="1;0"
              keyTimes="0;1"
              dur="1s"
              begin="-0.16666666666666666s"
              repeatCount="indefinite"
            ></animate>
          </rect>
        </g>
        <g transform="rotate(300 50 50)">
          <rect
            x="47"
            y="24"
            rx="2.52"
            ry="2.52"
            width="6"
            height="12"
            fill="#1dc071"
          >
            <animate
              attributeName="opacity"
              values="1;0"
              keyTimes="0;1"
              dur="1s"
              begin="-0.08333333333333333s"
              repeatCount="indefinite"
            ></animate>
          </rect>
        </g>
        <g transform="rotate(330 50 50)">
          <rect
            x="47"
            y="24"
            rx="2.52"
            ry="2.52"
            width="6"
            height="12"
            fill="#1dc071"
          >
            <animate
              attributeName="opacity"
              values="1;0"
              keyTimes="0;1"
              dur="1s"
              begin="0s"
              repeatCount="indefinite"
            ></animate>
          </rect>
        </g>
      </svg>
    </>
  );
};
export const Animation1 = () => {
  return (
    <svg
      width="329"
      height="69"
      viewBox="0 0 329 69"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <path
        d="M52.7998 49.6H24.2998C21.8998 49.6 20.0998 49.1 18.8998 48C17.6998 46.9 17.0998 45.5 17.0998 43.7C17.0998 41.8 17.6998 40.4 18.8998 39.3C20.0998 38.2 21.8998 37.7 24.2998 37.7H53.3998C56.6998 37.7 59.4998 37.3 61.6998 36.6C63.8998 35.9 65.5998 34.5 66.8998 32.7C68.0998 30.8 68.6998 28.2 68.6998 24.8C68.6998 20.1 67.4998 16.8 64.9998 14.9C62.4998 13 58.6998 12 53.6998 12H14.0998C11.6998 12 9.79981 11.5 8.59981 10.4C7.39981 9.29999 6.7998 7.89999 6.7998 6.09999C6.7998 4.29999 7.39981 2.8 8.59981 1.7C9.79981 0.599997 11.5998 0 14.0998 0H52.7998C57.3998 0 61.2998 0.499994 64.3998 1.39999C67.4998 2.29999 70.1998 4 72.5998 6.5C74.5998 8.7 75.9998 11.2 76.9998 14C77.8998 16.9 78.3998 20.2 78.3998 24C78.3998 28.5 77.8998 32.5 76.8998 35.7C75.8998 39 74.3998 41.7 72.2998 43.7C70.1998 45.8 67.5998 47.3 64.3998 48.3C61.1998 49.3 57.2998 49.6 52.7998 49.6Z"
        fill="white"
      />
      <path
        d="M25.6 19H54.1C56.5 19 58.3 19.5 59.6 20.6C60.8 21.7 61.4 23.1 61.4 24.9C61.4 26.8 60.8 28.2 59.6 29.3C58.4 30.4 56.6 30.9 54.1 30.9H25C21.7 30.9 18.9 31.3 16.7 32C14.5 32.7 12.8 34.1 11.5 35.9C10.3 37.8 9.7 40.4 9.7 43.8C9.7 48.5 10.9 51.8 13.4 53.7C15.9 55.6 19.7 56.6 24.7 56.6H64.3C66.7 56.6 68.6 57.1 69.8 58.2C71 59.3 71.6 60.7 71.6 62.5C71.6 64.3 71 65.8 69.8 66.9C68.6 68 66.8 68.6 64.3 68.6H25.6C21 68.6 17.1 68.1 14 67.2C10.9 66.3 8.2 64.6 5.8 62.1C3.8 59.9 2.39999 57.4 1.39999 54.6C0.499994 51.7 0 48.4 0 44.6C0 40.1 0.5 36.1 1.5 32.9C2.5 29.6 4.00001 26.9 6.10001 24.9C8.20001 22.8 10.8 21.3 14 20.3C17.3 19.5 21.1 19 25.6 19Z"
        fill="white"
      />
      <path
        d="M192.2 18.8L206.5 40.5C207.1 41.4 207.7 41.2 207.7 40.2V18.6C207.7 17.1 208 15.9 208.7 15.1C209.4 14.3 210.3 13.9 211.4 13.9C212.6 13.9 213.5 14.3 214.1 15.1C214.8 15.9 215.1 17 215.1 18.6V49.9C215.1 53.4 213.7 55.1 210.8 55.1C210.1 55.1 209.4 55 208.8 54.8C208.2 54.6 207.7 54.3 207.2 53.8C206.7 53.3 206.2 52.8 205.8 52.2C205.4 51.6 204.9 51 204.5 50.3L190.7 29.2C190.1 28.3 189.2 28.4 189.2 29.4L189.3 38.3C189.3 39 189.2 41 189.2 43C189.2 44.1 187.6 44.4 187.2 43.3L186.1 40.7C184.8 37.5 184.5 36.8 183.7 34.8L181.8 30.1C181.8 30 181.7 29.8 181.7 29.7V19.7C181.7 18.4 181.8 17.4 182.1 16.6C182.4 15.8 183 15.1 183.8 14.6C184.6 14.1 185.5 13.8 186.4 13.8C187.1 13.8 187.7 13.9 188.3 14.2C188.8 14.4 189.3 14.8 189.7 15.1C190.1 15.5 190.5 16 190.9 16.6C191.3 17.5 191.8 18.1 192.2 18.8Z"
        fill="white"
        stroke="white"
        stroke-miterlimit="10"
      />
      <path
        d="M136.5 38.7H129.1V50C129.1 51.6 128.7 52.8 128 53.7C127.2 54.5 126.3 54.9 125.1 54.9C123.9 54.9 122.9 54.5 122.2 53.7C121.5 52.9 121.1 51.7 121.1 50.1V19.4C121.1 17.6 121.5 16.4 122.3 15.6C123.1 14.8 124.4 14.5 126.2 14.5H136.5C139.6 14.5 141.9 14.7 143.6 15.2C145.2 15.7 146.6 16.4 147.8 17.5C149 18.5 149.9 19.8 150.5 21.4C151.1 22.9 151.4 24.6 151.4 26.5C151.4 30.6 150.2 33.6 147.7 35.7C145.1 37.6 141.4 38.7 136.5 38.7ZM134.5 20.4H129.1V32.6H134.5C136.4 32.6 138 32.4 139.3 32C140.6 31.6 141.5 31 142.2 30C142.9 29.1 143.2 27.9 143.2 26.4C143.2 24.6 142.7 23.2 141.7 22.1C140.5 21 138.1 20.4 134.5 20.4Z"
        fill="white"
        stroke="white"
        stroke-miterlimit="10"
      />
      <path
        d="M175.6 49.7L174 45.4C173.8 45 173.4 44.7 172.9 44.7H158.3C157.8 44.7 157.4 45 157.2 45.5L155.6 49.9C154.9 51.9 154.2 53.2 153.7 53.9C153.2 54.6 152.3 54.9 151.1 54.9C150.1 54.9 149.2 54.5 148.4 53.8C147.6 53.1 147.2 52.2001 147.2 51.3C147.2 50.8 147.3 50.2 147.5 49.6C147.7 49 148 48.2 148.4 47.2L158.6 21.4C158.9 20.7 159.2 19.8 159.6 18.7C160 17.7 160.4 16.8 160.9 16.1C161.4 15.4 162 14.9 162.7 14.4C163.5 14 164.4 13.8 165.5 13.8C166.6 13.8 167.6 14 168.3 14.4C169.1 14.8 169.7 15.4 170.1 16C170.6 16.7 171 17.4 171.3 18.2C171.6 19 172 20 172.5 21.3L182.9 47C183.7 49 184.1 50.4 184.1 51.3C184.1 52.6 183.4 53.7 181.9 54.7C181.8 54.8 181.6 54.9 181.4 54.9C180.2 55.1 179.2 55.1 178.5 54.7C178 54.5 177.7 54.2001 177.4 53.8C177.1 53.4 176.8 52.8 176.4 52C176.2 51 175.9 50.3 175.6 49.7ZM161.3 38.6H169.9C170.7 38.6 171.3 37.8 171 37.1L166.7 25.2C166.3 24.2 164.9 24.2 164.5 25.2L160.2 37.1C159.9 37.8 160.5 38.6 161.3 38.6Z"
        fill="white"
        stroke="white"
        stroke-miterlimit="10"
      />
      <path
        d="M257 38V45.6C257 46.6 256.9 47.4 256.7 48C256.5 48.6 256.1 49.2 255.6 49.6C255.1 50.1 254.4 50.6 253.6 51C251.2 52.3 248.9 53.3 246.6 53.9C244.4 54.5 241.9 54.8 239.3 54.8C236.2 54.8 233.5 54.3 230.9 53.4C228.4 52.5 226.2 51.1 224.5 49.3C222.7 47.5 221.4 45.3 220.4 42.8C219.4 40.2 219 37.4 219 34.2C219 31.1 219.5 28.3 220.4 25.7C221.3 23.1 222.7 20.9 224.5 19.2C226.3 17.4 228.5 16 231 15.1C233.6 14.1 236.5 13.7 239.7 13.7C242.4 13.7 244.8 14.1 246.8 14.8C248.9 15.5 250.5 16.4 251.8 17.5C253.1 18.6 254.1 19.7 254.7 20.9C255.4 22.1 255.7 23.2 255.7 24.1C255.7 25.1 255.3 26 254.6 26.7C253.8 27.4 252.9 27.8 251.9 27.8C251.3 27.8 250.8 27.7 250.2 27.4C249.7 27.1 249.2 26.7 248.9 26.3C247.9 24.7 247.1 23.6 246.4 22.8C245.7 22 244.8 21.3 243.6 20.8C242.4 20.3 240.9 20 239.1 20C237.2 20 235.6 20.3 234.1 21C232.6 21.6 231.4 22.6 230.3 23.8C229.2 25 228.5 26.5 227.9 28.3C227.3 30.1 227.1 32 227.1 34.2C227.1 38.9 228.2 42.5 230.3 45C232.4 47.5 235.4 48.8 239.3 48.8C241.2 48.8 242.9 48.6 244.6 48.1C246.2 47.6 247.9 46.9 249.6 46V39.5H243.4C241.9 39.5 240.8 39.3 240 38.8C239.2 38.3 238.8 37.6 238.8 36.5C238.8 35.6 239.1 34.9 239.8 34.3C240.4 33.7 241.3 33.4 242.4 33.4H251.5C252.6 33.4 253.6 33.5 254.4 33.7C255.2 33.9 255.8 34.3 256.3 35C256.8 35.6 257 36.6 257 38Z"
        fill="white"
        stroke="white"
        stroke-miterlimit="10"
      />
      <path
        d="M319.2 49.7L317.6 45.4C317.4 45 317 44.7 316.6 44.7H301.2L300.9 45.6C300.2 47.6 300.8 45.9 300.5 46.7C300.2 47.6 298.9 47.7 298.5 46.8C298.2 46.2 297.9 45.7 297.7 45.3C297.1 44.3 297.1 44.3 296.6 43.5C296.3 43 296.1 42.6 295.5 41.9C295.3 41.6 295.2 41.5 295 41.3C294.8 41 294.7 40.6 294.9 40.3L302.3 21.5C302.6 20.8 302.9 19.9 303.3 18.8C303.7 17.8 304.1 16.9 304.6 16.2C305.1 15.5 305.7 15 306.4 14.5C307.2 14.1 308.1 13.9 309.2 13.9C310.3 13.9 311.3 14.1 312 14.5C312.8 14.9 313.4 15.5 313.8 16.1C314.3 16.8 314.7 17.5 315 18.3C315.3 19.1 315.7 20.1 316.2 21.4L326.6 47.1C327.4 49.1 327.8 50.5 327.8 51.4C327.8 52.7 327.1 53.8 325.6 54.8C325.5 54.9 325.3 54.9 325.2 55C324 55.2 323 55.2 322.3 54.8C321.8 54.6 321.5 54.3 321.2 53.9C320.9 53.5 320.6 52.9 320.2 52.1C319.8 51 319.5 50.3 319.2 49.7ZM304.8 38.6H313.5C314.3 38.6 314.8 37.8 314.5 37.1L310.1 25C309.7 24 308.4 24 308 25L303.7 37.1C303.5 37.9 304.1 38.6 304.8 38.6Z"
        fill="white"
        stroke="white"
        stroke-miterlimit="10"
      />
      <path
        d="M273.2 37.2H271.5C270.9 37.2 270.4 37.7 270.4 38.3V50C270.4 51.7 270 52.9 269.3 53.7C268.6 54.5 267.6 54.9 266.4 54.9C265.1 54.9 264.1 54.5 263.4 53.7C262.7 52.9 262.3 51.6 262.3 50V19.4C262.3 17.7 262.7 16.4 263.5 15.6C264.3 14.8 265.5 14.4 267.3 14.4H280.4C282.2 14.4 283.8 14.5 285 14.6C286.3 14.8 287.4 15.1 288.5 15.5C289.7 16 290.9 16.8 291.8 17.8C292.8 18.8 293.5 19.9 294 21.2C294.5 22.5 294.7 23.9 294.7 25.3C294.7 28.3 293.9 30.6 292.2 32.4C290.9 33.7 289.2 34.8 286.9 35.6C286.1 35.9 285.9 37 286.6 37.5C287.3 38.1 288 38.8 288.7 39.6C290 41.1 291.2 42.7 292.2 44.3C293.2 46 294 47.5 294.6 48.8C295.2 50.1 295.5 51.1 295.5 51.6C295.5 52.1 295.3 52.6 295 53.2C294.7 53.7 294.2 54.1 293.6 54.4C293 54.7 292.4 54.8 291.6 54.8C290.7 54.8 289.9 54.6 289.3 54.2C288.7 53.8 288.2 53.2 287.7 52.6C287.3 51.9 286.7 51 285.9 49.7L282.7 44.3C281.5 42.3 280.5 40.8 279.6 39.8C278.7 38.8 277.8 38.1 276.8 37.7C275.8 37.3 274.6 37.2 273.2 37.2ZM277.8 20.4H271.5C270.9 20.4 270.4 20.9 270.4 21.5V30.2C270.4 30.8 270.9 31.3 271.5 31.3H277.6C279.5 31.3 281.2 31.1 282.5 30.8C283.8 30.5 284.8 29.9 285.5 29.1C286.2 28.3 286.5 27.2 286.5 25.8C286.5 24.7 286.2 23.7 285.7 22.9C285.1 22.1 284.4 21.4 283.4 21C282.5 20.6 280.6 20.4 277.8 20.4Z"
        fill="white"
        stroke="white"
        stroke-miterlimit="10"
      />
      <path
        d="M98.1 48.2H112.7C114.1 48.2 115.3 48.5 116 49.1C116.8 49.7 117.1 50.5 117.1 51.5C117.1 52.4 116.8 53.2 116.2 53.8C115.6 54.4 114.7 54.7 113.5 54.7H93C91.6 54.7 90.5 54.3 89.7 53.5C88.9 52.7 88.5 51.8 88.5 50.8C88.5 50.1 88.8 49.2 89.3 48.1C89.8 47 90.3 46.1 90.9 45.5C93.4 42.9 95.6 40.7 97.6 38.9C99.6 37.1 101 35.9 101.9 35.3C103.4 34.2 104.7 33.1 105.7 32.1C106.7 31 107.5 29.9 108 28.8C108.5 27.7 108.8 26.5 108.8 25.4C108.8 24.2 108.5 23.1 107.9 22.2C107.3 21.3 106.6 20.5 105.6 20C104.6 19.5 103.6 19.2 102.4 19.2C100 19.2 98.1 20.3 96.7 22.4C96.5 22.7 96.2 23.4 95.8 24.7C95.4 25.9 94.9 26.9 94.3 27.6C93.8 28.3 92.9 28.6 91.9 28.6C91 28.6 90.2 28.3 89.6 27.7C89 27.1 88.7 26.3 88.7 25.2C88.7 23.9 89 22.6 89.6 21.2C90.2 19.8 91 18.5 92.2 17.4C93.3 16.3 94.8 15.4 96.5 14.7C98.3 14 100.3 13.7 102.7 13.7C105.5 13.7 108 14.1 110 15C111.3 15.6 112.4 16.4 113.4 17.5C114.4 18.5 115.2 19.7 115.7 21.1C116.2 22.5 116.5 23.9 116.5 25.4C116.5 27.7 115.9 29.8 114.8 31.7C113.7 33.6 112.5 35.1 111.3 36.1C110.1 37.2 108.1 38.8 105.3 41.1C102.5 43.4 100.6 45.2 99.5 46.4C99 46.9 98.5 47.5 98.1 48.2Z"
        fill="white"
        stroke="white"
        stroke-miterlimit="10"
      />
    </svg>
  );
};
export const Animation2 = () => {
  return (
    <svg
      width="371"
      height="135"
      viewBox="0 0 371 135"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      xmlnsXlink="http://www.w3.org/1999/xlink"
    >
      <rect width="371" height="135" fill="url(#pattern0)" />
      <defs>
        <pattern
          id="pattern0"
          patternContentUnits="objectBoundingBox"
          width="1"
          height="1"
        >
          <use
            xlinkHref="#image0_1121_2449"
            transform="scale(0.00269542 0.00740741)"
          />
        </pattern>
        <image
          id="image0_1121_2449"
          width="371"
          height="135"
          xlinkHref="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAXMAAACHCAYAAADkzrkpAAAeTklEQVR4nO2d7XXcNtOGb78n/8NUELqCbCoIVIHXFZiuwFIFWlcguQIxFVipYJkKtKlATAXepwK+P0CEIBYgvvmxmuscnpWWJIAliMFgMBi867oOBDEjJYAnAH8BeFy2KARxPfzf0gUg3hS3AF4AMAAfli0KQVwXPy1dAOLNcABwL/3/90LlIIir5B2ZWYgZKAC89p8AcAbwvv8kCCIBZGYh5uAWgyAHgDuQICeIpJBmTuRG1cpbcK2cIIiEkGZO5EbVyr8uVRCCuGZIMydyUQE4ATiCtHKCyA5p5kQOduCeK3uQVk4Qs0CaOZEaYSO/A/AA0soJYhZIMydS8wBuXilBWjlBzAZp5kRKduArPD9jrJU3AG4WKhNBvAlIMydS8gBuTilBWjlBzAoJcyIVFXjMlW8AvkjfN/1BEERGSJgTKSgwaOUFSCsniNkhYU6kQCwMegZp5QSxCCTMiVhKcJ/yE4D/gbRyglgEEuZELE/gQbP+BPBJ+r4BaeUEMRskzIkYWH+c+v9L6Rxp5QQxI+RnTsTwCj7p+Re4rbzsv29AfuUEMSukmROhVODCuwU3s5TSOdLKCWJmSDMnQhDxV07g27/9AW5uASgGC0EsAmnmRAjCFbHtDyadI62cIBaANHPCF1Ur/w081C1AWjlBLAZp5oQvIoDW3+ATnXvp3J9LFIggCBLmhB8lhh2Ezhj7lZ8BPM5fJIIgAOCnpQtAbIr7/vMEblK5l87V4AJdhYGHxhW+6DuMV4meMXQOJ6Tntk9XxIypA9IowcsN8HI2CcoF8I7xA8bPowUf9TxD/zx9YBg8jsRnE5Fe2R8xaQB8NHfGEAIiFIbxfE0zcZ2gBn8OsVTIW3cyDFxxKqXvzlJeLQCg6zo66HA5dh3nteu6p67rDt2Y0nCfet0UP/q0TWmFHDJPgWkwKQ2WoEy7jj9H27M4RObDlDRjy37bp/MQmc6xT+cYmY7PuyUoIvN0rbvbyHzQl/Xo8Jueuq4jMwvhzEP/+QzgH4wDag3agZ0GXDv62v8t31eAazwvGDThlPybIU1fduCbXJfgI4aPAN71x3sM3kAF+OTymhAjp1vwelobjeVoMdZufVHr7gbjuhNmxgLcXTeGos+LgWvhn6W8fgHfllFo/3uAzCyEGwzDS1WAC0V5ePlt4l7VdKJbGboDj/EiBLh4kd8jfrgqyrwGCgDfMbh13mD8+1oAh/7zCXwYvVae+s864N5UJogGY1NfzlXH4p0sMAhyte7uwBWdJ/BV0TF8B28P5z4vuR2J+ammL9MzQBOghBuyrRwYT3yeMG1DdWm4JwC/Y/zCFhjbOkNJbYePSW+PQTP8CvOzqcE1sRh78hzIHbAP/6QuyAxUGJQCl7prIvLaYXj3a5jfOTGy+xMgYU7YYRiv7vwb4wY8pZX7oi44ymFqiSVGq/wg/d1Yrq2RZqIuN0ess55SI5tNGsu1NeLqjkl/20ZnTX+QMCesyFp5gbFAOiOt9pjSA2CNFIa/t4wwP1y7QJ+z7oLyImFOTMEwaAkF+PBYXiRUI60AVl/cJmHaa2Nvv2S1nHBpEhNzAT5stfOuZszrk/0SDglzYgrxIp3Bh40/K+dTmliAsYeMzRa/RWQBeI80cwJT5BKWukm5EsMEoStbsp230t9fkHckIufF4Nh5kDAnTJQYXiIxg19J5xuks+kW4JNprP9fuGJdG6qHwxHceyXXsF2dOGsSpq0T6MJ171pMSDKy7boAd589IM9vVU2XT+Ajn3LqJhLmhAnZ5euMy/09Q+OwMOV4AA/cVfXnG1x6tqSizZCmDw0uBeo9+O9/wvZML1sS6AXiylRjuu5YRNoqZ1w6A+z7vL7DoKmTMCd0iMU7gmdcxmGpA9M+Ksdtn5/Qxm+QT+jmSteHj7gUCuJ5f8e4Y9sCJoH+oL88Kz8AdIbjB+JNI1N1dwSvu1Qd8gH6WEd78M7jR3/Nf5AwJ3TcKv//i/EQr45Iu+kPVfMWppatCTNfhPD7DH3nUoI/hzVqtybEb5Jt9BWGhUVzkft5id8pr76UKcE75JDJYB13uOwoBQX4yOC/1dIkzAkd8kRkC+BX5XzMxOdNf/yOYWmyrPGU4EJgbkEwNzX4ClfT4iAGLhS2whoE+jvl+AXD+2YSiiE89mmb6m6PdHXXgLeVG+i9x/4za5Ewn58SfHhULloKMxXGWsUz8k18AoOv+g3GdsIKyjAyAWXi9FJQg3dm73E54mHY1ihFt8y9wnId8xnj2CypvXtqTNcdS5hXA955iPg98m8pANySMJ+fCsPEiWrOWANfLOdzbkBxwNgmaSuLL2Xi9FLSgjdW1Yvnw+Wlq2ZtAn0OWujrztlH3IMzeDtRn/EnEubzIy8LfgB/yddiG2UYTxKpWnnqFZ86VBewNawsFPXTzpBXjfEIJebdUJ/dXM9SCHSZCutUXlJSY1x3Zca8RFyW//IiYT4/pfJ/BT4zfTE7vQA6TUI1ueRetaebGE1FaEhZIQTbROWwofNiCEF9dqmepUs6J1xqqg/YltkohFR150IDqb2QMJ+XEubeWsxOL6WJqu6IZ1w22jn2+Ez9+2VNnwWmIUwdc4WkXfsyd9c6qnEp0J8wPM/YmN9rZO66+y8/EubzwhyuyWFnc6FS/m8wLm+LeZbXqw08Nk81hkjleT/DILzqyLKE0C6QZ0pqXAr0NZjO5qCdMy8S5vOw6w+XyaxbLDMUVTuRUvk/t60c4L+bSf/XCdJ8xrhR3cPP3CBWwjaIb5xPcLMbywtPYjc5WAM1lukIU+K6QreS/g4dyVZwW3RVYmgvf5Iwn4cC3BfUdXXY3MNP0dkIWlwK89RBtVRuMfZ4aMEXTaRAnZRyWZAjFjExpIsVw2A3pRUYGnKD9W9Q4cpnbFugi5WXtrqTO/8mMK8/4KbUiffkDOCRto3Lyw5c493BTxucexiqauVnjIX5CeFaKVP+Pyj//9ZfIz8fMVOfyv5YgzeQqv9/B+4aWoNrvo10LcPlbuh3iNfKSym9F/AO5hHj38jAG2jZfy97K1wDn8HrOdWSd7WdHCau/bm//g7+i4dKDO+nS92pnia+sP5TzC+o71/Z57WHvFgrwQ7SdKTdPVwwZzl/KHm/Kv/H7DTu8wxeu66rMv7OW81vtZVnlzB/3bM49seL9N1LF7+LPLquY0peLDK9SkorNI1C+q3HDM/TRmheD5q0jv3xqnwXW3dVd/mevnSX78lrJ72f77qui+hACAtid+0QbjDPhKO69PiES43nPcI1070mPYBr5C14NEaxUi/VcuspxCToH325SuV825flL+QxcZTgi6EYLp/Lc59vnSgv9dmfEPeb5PSeEV5fwuwotMpQKgyjyilPo18xHmWFlruEvu7E+5uy7sR7+gGXMkTO67/RAQnzvMQI8zvM47P6hGmXxBN4bAiCIFYMTYCul9AFLr7I9sulfMsJgoiEhPl6KWfIYw/75rHX4k1BEFcNCfO3jc0FMsaLhSCIGSFh/raxuYiRiYUgNgIJ87eLzpNDhUwsBLERSJi/XZjlPJlYCGJDkDB/u9jixDRzFIIgiDSQMH+7MMt5spcTxIag2CxvE2Y5f8Y8qzEJYikK6Fcmn7D+ePJaUgnzEnwVYYN5h+eHxOm1/bF0hYoy5IJZzodOfO7APWRilnr7UoK/e2fMu8tLhfEEcgO3d59hiKwnk3Lji9+gX/xVJ8xDpsQy7d+VEvy5ixAOLoHshEJzAvAP0m5kzjC8AyfwkBahiFAFbeoAQq+J0nM9cvLa8eA6McGJjoF5HyLyTFGufWC63/v7nzKXXz6YVO5qxnzVZ+j6zFi3DDHvse0QQaheMuYRclRdeBvU8dr/1hTB11JzTBWb5RWDljJXgCgAmCuwTAse9rL2vO87wsJ91kgTP9uE7bn9Av+RSQn+HsSkEYKa71xxs9W4O67vPevvnZuc7fIHhpHAnO3fxB5DOFoTYvQtj4hEmFzmkEeDuNFOatnVpDCz7DF+aJ8wX2XqYomY7L1n8OGSQF79yCz5lOABqe7BhUXjWL5/ECbMc24EYRtiNggTwpXm/znMHq3y/xOG4XFOUnRUruY8XTx8l3kN2S7sE0/fh0pJe872r1JgekegBlwAP8P+3PfgHl+V4Tzrjw9IF3e+hZ8pZ/xeJBgufFf1/S5NLOaQoW7XhcdI3nU83rUuTRXX+N4Hh7R0hJo5XIeeUxwC031V0nnN+BtsQ9YfXdo45C51yxzvC2kbR81vPHqmUWZ6DrqyzdX+5WPXmWPVv3ThZqai08cyFzwEpqvjEPP7Y10TS+h7wSoy3SU4gWuSN+Dxu+uJax8w3uLMRKj2lnNTZ1s0xpDJT3V0BpjfjTkQ8bJzaaMxLDWx3mZIs4R+VFtlyGuKHcz1/RU8hHMTmPYZPBz179CPhNbiwnuKFeZfPL/fCi24OeUG5kZQwe5NEzrUZ4H3uTBlZgl1STTVd85OycbcAn2T7myRrKH9Twnyz0jn8Sbi+tfSd6ty4Y0V5pXh+xLLaWUpG1UDc48McBs6s9wfUp4C+ULgsolzTUB65USaOo19TqYaempW06hnQuyEo6NEXoVEzmdKkNcZ8pTTbSLSiblXS4wwrzDdSJbSyv6xX+KF2NqqNZy3mVtCfbbLwPumsHWwIb7ONi2sCkgzJTu4mcQIP9RY+CpzaOdPhjKEeJ758Bnc9JLTUcGbGGEuV5ZOK1laK0vJGWZXwRLTAmtNFW6Lx+Lb8ajaWaO5Zg0mtz1IoKdGXvjUaM7nbv+30Gv/J6RfTKjjEcu7YI4IFeYMg+21BddcdeaENTTkVDQw9/a6FX2C0OiDqU0DBaY18xb+5ZS1s2dwbUWXb+WZbg4qXJdAX9KswzAI6hbcNU/X/qtM+RcwtzndO/gmCBXmsgnlK3hF1prrKqzToyCUr4bvS0wLyhBTi8uSYx9sw+ImIE25QX0DFzC6dHJ36q309x3M8xQV5tHa5iBmCXgscn2K9q97x3PV+y3073KDlWnLcxIizEsMPa5ciTpzgk0b3BotzC/L1BZsa3BfsnWqf3mmxzBoZw2G56L7rTuk75xkWunvE8wjRYB3QFXGslw7JYY2LStxOkUn16jMNB+3JpPm7IQI80r6+xuGRtNC3ztPmSC2iEnosYl7Qk0tKbEJ88YzPbleZQFeQ/9b5zS52QT6E0ighyLXoyw8W8wzKttBb4s3jQ7eDL7CvMC4cmrlvK5nLDGPm9JcmGyVNs3T18b5s+f1NqYmPxv4uVCWGOq0hdt7UGFek9sJ0/FtSKD7o2raj8p5Xb3vkLb9m7TyNy3IAX9hLttda1xqYI3mO+D6JkJNTAl04TLZwi44z5a0fCks6fm6JKq2cpUa+t9465lPLM+YFugPyGv+uTYqjNu/WsfP0Lf/lG7KpvpKGUJ4k/gKc1sjBvS2s2tyUwTMwthF82xht+2l1mBt8xY+Wo2snZkmvk1D3iXWHtQwC3SxSpQEuhvqxKcO3fcV0rV/Zvi+TZT+ZvER5rJAbmA2Gzzj+t0UQ9zCnjEMS5sM6U8xJUR9lyTL2nUNc8ema9QlljFt1CCBHkuFcftvDdeZ2n+VoAzlxLkmQfpbocQQtXHff3oJc9PEh8rZcL7Cdbkp6pgSCCdwtzkR73nK1LJDOoFeYtpm6WtrdH0PWqxrEVENEugxyAqBrf3Xmu9T1HuZII01cw8e59x2vIK/r0fwPRMAuAtzhvGEl00A1Jrvrs1NUYdPZ2V7hqnCEtgakY+tscJ4kVBruV6nnaeeEPOhhnlRSQHz8vC3DoNf+ze5KVepCqTQZkp3U7gKc9deWdBCL9CvzU1RxUeb9vXrDsXWgTYeabnMmahpt5rvl4ym+AjzSt4d1hs6d0lcR2OCFvMuImozpbspXHYaKjHuUVvwXrrF5UNkDmkxbN++xQzf+7j32bSb1iMtEyafXIGP/7tuEptBv1sOk/5uNfdV4Fq7a96pEeaWSnNOCPQpP/W3RImxQtDCrf3rnp0YlTUpCjYzskdYo/k/BTX8Fhiy/vMMuAlztTf9rr3KnXtsszJz0MDcMZQJ0k+plavvQew+lhWWXVpPAt0NdTQd2/6/IH37n2Ouo4T+nT+D73ebgn/h92xG19rMLDnsXAzbnsiwTXL6MGWvLj3TCsHV1MOQ3s79BcubM6ZiXpPJJU/7z+GmPEcdmdp2as+zYGzC/BZ5HtSWbeel4fsW/lrc1IuQYgWozTbdJEonhFwT4qXn9Z8xvao3VhPdMrkWeYXaztuJc2+50wVgF+bq0v2P4EPPXwC80xzv+/Pi+ApzrOOtPnxTQK2QHnrqntihY4VpwebqklhirJ19xVC/pvfgd+maj/09ut+ao1MvA+65gbkuGK4rdK4rBfK1/wph7b+dODeHqaWZIY9gpmzmFYYH3mJ6WbSgxfiBN+CN61W5rgDv9Q8Oaa4NZvg+xDulBdfmdS92GZCeoABfqj6Fq0uiugnBweEeVTAKN0ZVKJZYx4TYGVz4mHzNq/7TpQ1cC7LC1SK8/e8AvCjXCfPNI/xpoG+DDMu8R5sws5ii4vnSQv+Ql3RPC6WEWQPwXXwjML0MZWB6gJu/tEt5VZtpTIjRGnrNai0mNyHQTfVR4W1p6L5uqCZOSLt4zKSE2HbRysWSceVHmIR5hbEwCelBZUzRFKvIdOemMnxvWsLswpSGHDJ03MNui27h5hYo20xbxEem0ykFDOuZED/DvGsO8HYEeoWhTkwrOn3Q1XuJsPZvegdtbrhXj0mYq7ayWPcs02rBLcVrUW2IMrGay1SePuzgJmwah2vU35si8H9t+D5WOy8j75dpMe2SWGEdAv23jGnL9RGjqAhqQxoho/MTzG1mS/IkOTphzjDWCFPt3mHagYYlSj8398izVdWUMGce6RRwd6Vzse+rnky1R1lMtIZ0KsQJ5Jh7dZxgF+hLC45cDgSq62Cq9q9LhyGs/ZvKVOENa+c6YS73ylO9oC8mU81abKZTMOjdtM6InxRrYRYaPtpXBfcG3jhck3p0JjDNv1SJ0k/FCdzkYmKr3lg25HpP2f5rh/x80mo037tM/F8tqjBnGPeUKffUM9neGNbdm5Yw+xrfIc2S9BSToK1HXjbBXGEsrGImwFUamE1uaxOQDd6WBwtDvvbfQt/+QxcRmQKm7bE+xWAWVGEua8lnpN+KySQU1qqdT60CfEQa0wNgngTdwX0S1DQvoeJiYpHro0V6ly9dNMUcqw1TUOPtCPQttf8TzAL9CW8wnLEszBnGvXKKiQ+VBnottEI67TzVxNAeXJCXmnM1zC9SCM3EOddhaAW3ZziVly6dHDuem96tpe3QJmrEe3StHYZ52n+r+b5CWPufUqiOWPd8HEudoCzM1d4xRyOeSjeVdh47VGcYgr7r0rpDek2tmThXOabhIgjPlryAy3qoHfP3waT1lYjXzn+NvN/EHaafxdpMRL4s3f6rwPRM8XWEQ8AhMF1XcnoVeSGEOcNl2NJcK5tMQ7ellvgX4L/9AcMOHkxzXQvu4ZBLQ2si7mVwG1ba8qgw1pBq5IsamKtTLyPvn8IWmGurMIzf+ZQTnyq14fuYOZPPMO9Jeg++ApUFpi1g0L9bS3biQnYxAHshzNUGlNpWJmPSygr4B/Zhhu/kLZZeMGyxJB+v/fkf/f+30FfWGfxF+R15lwubGo9LnswxD5u9XH0Pcm6gcYJ+yF1inbZzwR3mX8Kdu6NQ6z3lhLdKyvYvcwBXtlrNOTH39drn4fI8S3AF8wmDkldGlM8Fdds4ke8P6LePE7LrCOALuq5j3SW7ruuQ8ag0eXZd1/3ouq7wSCcnr13X3XqWJ+bYG8rxw+Heg+NvKifS0NVJ7t/8YCjn0SONMuLe0KPouu5FyZdlzE/Ha6K0mSbtrbR/U90c+rRsHA2HD98Dy5ma47uu61Szwhnpgq2bKPpeRcdXuNu5uiSl4bTgGtffME/U5mTqmbyz3HuA3TzRgke1M+X9grHm0YBrOjlhMG9ycQP3UYmcxgl8FJWbEvyZiWG2a3lDML3ntvfChbW1/ylTlm8eFbj5pkyQnqAFr+e/ETdJnFJ2AUDzE7jt0mRvyoUtBoYrvmkU4EMs1de6iSxHCs7g5dINARmmy+gyCTNlOitwOamby1Yu08Bch675txh3OnPZMEW+c2xgoXtGKfIsse32b8vjsT9KcJPJbxiC5U09P9EW0X/+i2EeIUWZd+DtUd60vfFMo8TQSf0MAO+6LnUHQUTwBL29+A7TE68/YG/cH5F3LoQgiAWxbU5BzItp8ZBpQwzA3Quo8S4NQRCbgYT5ujDZ6adm313iODegjYkJ4qohYb4upmK0lBPnbOR0MSQIYgWQMF8fjeF7Zvi+jEiTIIgrgYT5+vC1m5eW9FqsaJ9CgiDyQMJ8fZgE717zXemQXhNcEoIgNgMJ8/VhWohQ4FKglw7pkb2cIN4AJMzXickfPGQH8iaiHARBbISfkD9EJGGmhj4w0F/QLx7aY7xSk1nSb+DuknhwvI4giBVCwnydCFOLuhhImFqE5m6L3W2aTNVx8LiWIIiVQWaW9eJiamGBaRAEcWVQbJb1sod5I+lfwLX014n754h+RxDESvhp6QIQRpqJc7ewb948dT9BEFcGmVnWi2lHFgD4BLtni4+9nCCIjUNmlnVTgYfFDeE97No7QRBXAgnz9eMSq1ylhXlXIYIgrhAys6wf0y72UzSpC0EQxLohYb5+6oB7cu6uTnBcdngnxhTwG2WWnte/aUiYr58Wfv7iLebTzI/gG9Oqxyv4IiRbQ9Td2/Xp6gKLqRz66w+G84WUJrOkte+vqxzyLXC5EbIJBvPvVA8Th8D7fBB1yTzvE8/NtDG3zC2AB4+0n+DWacY+v9g6sP12W55HuL13k5Aw3wY+ppYltPKv4DvFv8OwUe893Bo4pHvfg4cr2IH72LsI9CnOGEY2nyzXflCun6ICF+i2NAHeud6Bb/4sDoH83dSmyiIkw0m557Plvjnw3cGqgluAOAb/jsVEA/6sxHO7679Xn6fLs6xxWQeube4XDO/6OwC/92UpwTuuF8SMRLquo2Mbx2vnRjljmY59nkz5vui67ofhnHwI1O+r/vujJf9Df91h4hrWX/Nj4prCIR1TXRQBz63r03C9XvyG7wF5xdala9mOHnk8eTxjlzoR5CizOA4e5fEtX9F13Ut/TXAdk2a+HVy1hjZvMUaYtLIzhrjsLCDdtv9MYS9t+rIUMA9lxQigdkhPXNv0n6Y0bbQB9/wTmNdaEPVZYVo7t52/Ns7gI1qAv19B8zEkzLdDDfuQdu4h95RwEQ03ZJcjcW8bcK8OYaYyLbT6BD4v4ZLfF/Bh9Tfpf8JtkdoOwzO+n7juHsN781tEmbZEi0FBCDIvkjDfFlPCusZ6FglV4A33DP/J2BJDQ0/VOYkolHtcanwl+OjBxe4prn3EIPzFd8Q0ooMW9uoKeu370F8rrluLN0szQx6iAzNtETkJCfNt8dgfKmcsOxG2Axdoe/CJnKe+THdwmyA79McR3BPmDD45lGrvUjk0QqWcq+DuMXSP8QhJaOcuE6EpuMelJwRLnEeTOD2BMB08Y3hXVe28AB/pfJXKUWYqTyg56+B/MTdToK3tcQdu3ngAf/lbcHtbu1yRLmLF3MHNLCQ4Y3BZ+4g8oXu/gQvuTxi7Mn6Cm1YubO61dP/P/WcFLoDauCJaqXFZ1q1s1l1ieB8ewYV2hfFzu+2vEQrLGesT5jVWWgckzLdJDf4C3YO7Rvm6h6XmDnEa3SO4sLwH1+obpP9Np/7YYdjgQ5hdaof7haApMRYwbf9/hfwbfPyL7a7uLTEIvTN45yrq+waDee2jdM8JXOstsPw7LshZB0I5aENuJjPLdjlh8Om+Bg7gjaSAOY57LEKj+iB9ukx8iuG/6i9+g0H4uJpaSsfrliLXylZ1IvMR/Lmz/rgHr3/dqGwNq23ZjHkERTwlYU7EkNrTQJiLGPxWCrpS958VuICo4GZiEd4FOkEjNP4Sbm6KZf+5BgGlI3TC8WfL+QJjISXP8zxhMLnItIFl2SIMg9NAkJmRhDkRQ2pPA9nf9hYJljhr0q/7v7/Db+JTeMToCJkIDXl2tj1fl8TWOTHNdzUGM1WDS/PFvxP3XhNixTPg7jRwAQlzYm2cMGhoD0ivwf7Vf5ZwE+RVf+1USAWRDkMejXsnfTLliA15MEWqPKaeiajrrQSH+xWXdVAFpsXA33GxjP8zwgLrAaAJUCINqTX0A7g9ewfurvge6eYGZP9wl5g3n/rrpzwWxNB4D25b/xxTQA3i+YrnofIuYV669BuYOz6XuhfCXFeHzxhcPlXa/jPI79qBkPe2gl541w736gJ5teDzB98QaVb6f/q79uP6EugRAAAAAElFTkSuQmCC"
        />
      </defs>
    </svg>
  );
};
