import { NextPage } from "next";
import { useEffect, useState } from "react";
import WebSiteUserLayout from "../../layout";
import Card from "../../components/Card";
import { daysLeft } from "../../utils";
import { useStateContext } from "../../context";

const Profile: NextPage = () => {
  const [isLoading, setIsLoading] = useState<boolean>(false);
  const [campaigns, setCampaigns] = useState<any[]>([]);
  const { address, contract, getUserCampaigns } = useStateContext();

  const fetchCampaigns = async () => {
    setIsLoading(true);
    const data = await getUserCampaigns();
    setCampaigns(data);
    setIsLoading(false);
  };

  useEffect(() => {
    if (contract) fetchCampaigns();
  }, [address, contract]);

  return (
    <>
      <WebSiteUserLayout>
        <Card
          title="My Campaigns"
          isLoading={isLoading}
          campaigns={campaigns?.filter(
            (campaign) => Number(daysLeft(campaign?.deadline)) >= 0
          )}
        />
      </WebSiteUserLayout>
    </>
  );
};

export default Profile;
