/** @type {import('tailwindcss').Config} */
const defaultTheme = require("tailwindcss/defaultTheme");
module.exports = {
  content: [
    "./app/**/*.{js,ts,jsx,tsx,mdx}",
    "./pages/**/*.{js,ts,jsx,tsx,mdx}",
    "./components/**/*.{js,ts,jsx,tsx,mdx}",

    // Or if using `src` directory:
    "./src/**/*.{js,ts,jsx,tsx,mdx}",
  ],
  theme: {
    container: {
      center: true,
      padding: "1rem",

      screens: {
        xl: "1350px",
      },
    },
    screens: {
      xs: "380px",
      ...defaultTheme.screens,
    },
    extend: {
      fontFamily: {
        epilogue: ["Epilogue", "sans-serif"],
      },
      boxShadow: {
        secondary: "10px 10px 20px rgba(2, 2, 2, 0.25)",
      },
      colors: {
        primary: {
          light: "#15D9B6",
          DEFAULT: "#0DAA8E",
          dark: "#098A73",
        },

        secondary: {
          light: "#14C5DD",
          DEFAULT: "#0D97AA",
          dark: "#0B8090",
          xDark: "#005974",
        },

        error: {
          DEFAULT: "#ec1c24",
          dark: "#a01319",
        },

        info: {
          light: "#0096c7",
          DEFAULT: "#0077b6",
          dark: "#023e8a",
        },

        warning: {
          light: "#ecec5f",
          DEFAULT: "#c6c651",
          dark: "#ffff00",
        },

        textColor: "#212121",
        dark: "#1E1E1E",
        light: "#EEF5FF",
      },
    },
  },
  plugins: [],
};
