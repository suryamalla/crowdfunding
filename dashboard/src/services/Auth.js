import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react';

export const apiSlice = createApi({
  reducerPath: 'api',
  baseQuery: fetchBaseQuery({
    baseUrl: process.env.REACT_APP_PUBLIC_URL_BASE, // Make sure your environment variable is set properly
    prepareHeaders: (headers) => {
      const token = localStorage.getItem('token_crw') || ''; // Get the authentication token
      if (token) {
        // If a token exists, add it to the headers
        headers.set('Authorization', `Bearer ${token}`);
      }
      return headers;
    },
  }),
  tagTypes: ['Login', 'Name', 'singleAdmin', 'allAdmin',"allFeedbacks"],
  endpoints: (builder) => ({
    LoginAdmin: builder.mutation({
      query: (data) => ({
        url: '/auth/login',
        method: 'POST',
        body: data,
      }),
      invalidatesTags: ['Login'],
    }),
    allAdmins: builder.query({
      query: () => ({
        url: '/users',
        method: 'get',
      }),
      providesTags: ['allAdmin'],
    }),
    allFeedBacks: builder.query({
      query: () => ({
        url: '/feedbacks',
        method: 'get',
      }),
      providesTags: ['allFeedbacks'],
    }),
    DetailAdmins: builder.query({
      query: (id) => ({
        url: `/users/${id}`,
        method: 'get',
      }),
      providesTags: ['singleAdmin'],
    }),
    LogOutAdmin: builder.mutation({
      query: (data) => ({
        url: '/auth/logout',
        method: 'POST',
        body: data,
      }),
      invalidatesTags: ['Login'],
    }),
    CreateAdmin: builder.mutation({
      query: (data) => ({
        url: '/users',
        method: 'POST',
        body: data,
      }),
      invalidatesTags: ['allAdmin'],
    }),
    updateAdmin: builder.mutation({
      query: (objData) => {
        return {
          url: `/users/${objData?.id}`,
          method: 'PATCH',
          body: objData?.data,
        };
      },
      invalidatesTags: ['allAdmin'],
    }),
    deleteAdmin: builder.mutation({
      query: (id) => {
        return {
          url: `/users/${id}`,
          method: 'DELETE',
        };
      },
      invalidatesTags: ['allAdmin'],
    }),
  }),
});

export const {
  useLoginAdminMutation,
  useLogOutAdminMutation,
  useAllAdminsQuery,
  useCreateAdminMutation,
  useDetailAdminsQuery,
  useUpdateAdminMutation,
  useDeleteAdminMutation,
  useAllFeedBacksQuery
} = apiSlice;
