import React from "react";
import WebSiteUserLayout from "../../layout";
import ContactForm from "../../container/FeedBack";

const AboutUs = () => {
  return (
    <>
      <WebSiteUserLayout>
        <div className="2xl:container 2xl:mx-auto lg:py-16 lg:px-20 md:py-12 md:px-6 py-9 px-4">
          <div className="flex flex-col lg:flex-row justify-between gap-8">
            <div className="w-full lg:w-5/12 flex flex-col justify-center">
              <h1 className="text-3xl lg:text-4xl font-bold leading-9 text-white dark:text-white pb-4">
                About Us
              </h1>
              <p className="font-normal text-base leading-6 text-white dark:text-white">
                Welcome to Crowdfunding, where innovation meets opportunity. Our
                platform harnesses the power of blockchain technology to
                revolutionize the way projects are funded and dreams are
                realized
              </p>
            </div>
            <div className="w-full lg:w-8/12">
              <img
                className="w-full h-full"
                src="images/about1.jpg"
                alt="A group of People"
              />
            </div>
          </div>
          <br />
          <div className="flex flex-col lg:flex-row justify-between gap-8">
            <div className="w-full lg:w-5/12 flex flex-col justify-center">
              <h1 className="text-3xl lg:text-4xl font-bold leading-9 text-white dark:text-white pb-4">
                Our Vision
              </h1>
              <p className="font-normal text-base leading-6 text-white dark:text-white">
                At Crowdfunding, we believe in the democratization of funding.
                We envision a world where groundbreaking ideas, whether they're
                from established visionaries or emerging talents, have an equal
                chance to thrive. With blockchain as our cornerstone, we're
                building a global ecosystem that transcends geographical
                boundaries and financial constraints.
              </p>
            </div>
            <div className="w-full lg:w-8/12">
              <img
                className="w-full h-full"
                src="images/about.jpg"
                alt="A group of People"
              />
            </div>
          </div>

          <div className="flex lg:flex-row flex-col justify-between gap-8 pt-12">
            <div className="w-full lg:w-5/12 flex flex-col justify-center">
              <h1 className="text-3xl lg:text-4xl font-bold leading-9 text-white dark:text-white pb-4">
                Contact Us
              </h1>
              <p>
                Contact No.: 094-690521 <br /> Email: suryamalla55@gmail.com
              </p>
              <p></p>

              <p className="font-normal text-base leading-6 text-white dark:text-white">
                It is a long established fact that a reader will be distracted
                by the readable content of a page when looking at its layout.
                The point of using Lorem Ipsum.In the first place we have
                granted to God, and by this our present charter confirmed for us
                and our heirs forever that the English Church shall be free, and
                shall have her rights entire, and her liberties inviolate; and
                we will that it be thus observed; which is apparent from
              </p>
            </div>
            <div className="w-full lg:w-8/12 lg:pt-8">
              <div className="grid md:grid-cols-4 sm:grid-cols-2 grid-cols-1 lg:gap-4 shadow-lg rounded-md">
                <div className="mapouter">
                  <div className="gmap_canvas">
                    <iframe
                      className="gmap_iframe"
                      frameBorder={0}
                      scrolling="no"
                      src="https://maps.google.com/maps?width=600&amp;height=400&amp;hl=en&amp;q=nepal%20tinkune%20cite&amp;t=&amp;z=13&amp;ie=UTF8&amp;iwloc=B&amp;output=embed"
                    ></iframe>
                    <a href="https://gachanox.io/">Gacha Nox Download</a>
                  </div>
                  <style>
                    {`.mapouter{position:relative;text-align:right;width:600px;height:400px;}
        .gmap_canvas{overflow:hidden;background:none!important;width:600px;height:400px;}
        .gmap_iframe{width:600px!important;height:400px!important;}`}
                  </style>
                </div>
              </div>
            </div>
          </div>
          <ContactForm />
        </div>
      </WebSiteUserLayout>
    </>
  );
};

export default AboutUs;
