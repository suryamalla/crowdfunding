import React, { useEffect, useState } from "react";
import WebSiteUserLayout from "../../layout";
import Card from "../../components/Card";
import { useStateContext } from "../../context";
import { daysLeft } from "../../utils";

const AllCampaigns = () => {
  const [isLoading, setIsLoading] = useState<boolean>(false);
  const [campaigns, setCampaigns] = useState<any[]>([]);
  const { address, contract, getCampaigns } = useStateContext();

  const fetchCampaigns = async () => {
    setIsLoading(true);
    const data = await getCampaigns();
    setCampaigns(data);
    setIsLoading(false);
  };

  useEffect(() => {
    if (contract) fetchCampaigns();
  }, [address, contract]);

  return (
    <WebSiteUserLayout>
      <Card
        title="All Campaigns"
        isLoading={isLoading}
        campaigns={campaigns.filter(
          (campaign) => Number(daysLeft(campaign.deadline)) >= 0
        )}
      />
    </WebSiteUserLayout>
  );
};

export default AllCampaigns;
