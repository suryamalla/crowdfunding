import { Typography } from '@mui/material';
import PageContainer from 'src/components/container/PageContainer';
import DashboardCard from '../../components/shared/DashboardCard';
import UserCreate from 'src/components/user-form/UserCreate';

const CreateUser = () => {
  return (
    <>
      <PageContainer title="User Management | Crowdfunding" description="">
        <DashboardCard title="User Management">
          <UserCreate />
        </DashboardCard>
      </PageContainer>
    </>
  );
};

export default CreateUser;
