/** @type {import('next').NextConfig} */
const nextConfig = {
  reactStrictMode: true,
  basePath: "",
  images: {
    domains: ["static.toiimg.com"],
  },
};

module.exports = nextConfig;
