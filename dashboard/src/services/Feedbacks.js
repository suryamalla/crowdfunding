import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react';

export const apiSlices = createApi({
  reducerPath: 'apiFeed',
  baseQuery: fetchBaseQuery({
    baseUrl: 'http://localhost:1337/api', // Make sure your environment variable is set properly
  }),
  tagTypes: ['allFeedbacks'],
  endpoints: (builder) => ({
    allFeedBacks: builder.query({
      query: () => ({
        url: '/feedbacks',
        method: 'get',
      }),
      providesTags: ['allFeedbacks'],
    }),
  }),
});

export const { useAllFeedBacksQuery } = apiSlices;
