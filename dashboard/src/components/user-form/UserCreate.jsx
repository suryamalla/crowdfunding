import React, { useEffect, useRef, useState } from 'react';
import { Field, Form, Formik } from 'formik';
import {
  useCreateAdminMutation,
  useDetailAdminsQuery,
  useUpdateAdminMutation,
} from 'src/services/Auth';
import { toast } from 'react-toastify';
import { useNavigate, useParams } from 'react-router';

const UserCreate = () => {
  const { id } = useParams();
  const [createAdmin, { isSuccess, isError, error }] = useCreateAdminMutation({
    skip: id,
  });
  const [updateAdmin, { isSuccess: updateSuccess, isError: updateIsError, error: updateError }] =
    useUpdateAdminMutation({
      skip: !id,
    });
  const { data: DetailData } = useDetailAdminsQuery(id, {
    skip: !id,
  });
  const [detailData, setDetailData] = useState({
    name: '',
    email: '',
    password: '',
    role: 'admin',
    isPermission: 'false',
  });
  useEffect(() => {
    if (id && DetailData) {
      setDetailData({
        name: DetailData?.name,
        email: DetailData?.email,
        // password: DetailData?.password,
        // role: DetailData?.role,
        isPermission: DetailData?.isPermission ? 'true' : 'false',
      });
    }
  }, [id, DetailData]);

  const navigate = useNavigate();
  const formikBag = useRef();
  useEffect(() => {
    if (isSuccess) {
      toast('User careated successfully');
      formikBag.current.resetForm();
      navigate('/user-list');
    }
    if (isError) {
      toast.error('Something went wrong');
    }
  }, [isSuccess, isError, error]);
  const handleSubmit = async (values, { setSubmitting }) => {
    try {
      setSubmitting(true);
      if (DetailData && id) {
        const objData = { data: values, id: id };
        await updateAdmin(objData);
      } else {
        await createAdmin(values);
      }
    } catch (error) {
      console.error('Error:', error);
    } finally {
      setSubmitting(false);
    }
  };
  return (
    <Formik
      initialValues={detailData}
      onSubmit={handleSubmit}
      innerRef={formikBag}
      enableReinitialize
    >
      {() => (
        <Form>
          <div className="form-wrapper">
            <div className="form-group">
              <label htmlFor="name">Name:</label>
              <Field type="text" name="name" className="form-control" autoComplete="off" required />
            </div>
            <div className="form-group">
              <label htmlFor="email">Email:</label>
              <Field
                type="email"
                name="email"
                className="form-control"
                autoComplete="off"
                required
              />
            </div>
            {!id && (
              <div className="form-group">
                <label htmlFor="password">Password:</label>
                <Field
                  type="password"
                  name="password"
                  className="form-control"
                  autoComplete="off"
                  required
                />
              </div>
            )}
            <div className="form-group">
              <label htmlFor="isPermission">Permission:</label>
              <Field
                as="select"
                name="isPermission"
                className="form-control"
                autoComplete="off"
                required
              >
                <option value="true">Yes</option>
                <option value="false">No</option>
              </Field>
            </div>
          </div>

          <button type="submit" className="btn btn-primary user-submit">
            {id ? 'Update' : 'Create'}
          </button>
          <button type="reset" className="btn btn-primary user-reset">
            Clear
          </button>
        </Form>
      )}
    </Formik>
  );
};

export default UserCreate;
