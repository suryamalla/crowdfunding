import { NextPage } from "next";
import React, { ReactNode } from "react";
import { logo, menu, search, thirdweb } from "../public/assets";
import Link from "next/link";
import {
  AboutIcon,
  CampAllIcon,
  CreateIcon,
  HomeIcon,
  ProfileIcon,
} from "../utils";
import { useRouter } from "next/router";
import { useStateContext } from "../context";
interface HeaderProp {}
const Header: NextPage<HeaderProp> = () => {
  const navigate = useRouter();
  const { connect, address } = useStateContext();
  // const handleClick =async () => {
  //   if (address:any) navigate.push("/create-campaign");
  //   else await connect();
  // };
  return (
    <>
      <nav className="nav-header">
        <ul className="ul-header">
          <li>
            <Link href="/">
              <HomeIcon /> Home
            </Link>
          </li>
          <li>
            <Link href="/all-campaigns">
              <CampAllIcon /> All Campaigns
            </Link>
          </li>
          <li>
            <Link href="/profile">
              <ProfileIcon /> Profile
            </Link>
          </li>
          <li>
            <Link href="/about-us">
              <AboutIcon /> About
            </Link>
          </li>
          <li>
            <Link href="/create-campaign">
              <CreateIcon /> Create Campaign
            </Link>
          </li>

          <li>
            <a
              className={
                !address
                  ? "codepen-button"
                  : "codepen-button codepen-button-hover"
              }
              onClick={() => {
                address ? "" : connect();
              }}
            >
              <span>
                {address ? "Connected With Wallet" : "Connect Wallet"}
              </span>
            </a>
          </li>
        </ul>
      </nav>
    </>
  );
};
export default Header;
