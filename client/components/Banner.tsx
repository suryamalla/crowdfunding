import { useRouter } from "next/router";
import React from "react";

const Banner = () => {
  const router = useRouter();
  return (
    <>
      <section className="relative bg-banner bg-cover bg-center bg-no-repeat">
        {/* <section className="relative bg-[url(https://images.unsplash.com/photo-1604014237800-1c9102c219da?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1770&q=80)] bg-cover bg-center bg-no-repeat"> */}
        <div className="absolute inset-0 bg-white/75 sm:bg-transparent sm:from-white/95 sm:to-white/25 ltr:sm:bg-gradient-to-r rtl:sm:bg-gradient-to-l"></div>

        <div className="relative mx-auto max-w-screen-xl px-4 py-32 sm:px-6 lg:flex lg:h-screen lg:items-center lg:px-8">
          <div className="max-w-xl text-left ltr:sm:text-left rtl:sm:text-right">
            <h1 className="text-3xl font-extrabold sm:text-5xl text-black">
              Let&apos;s bring joy to individuals and
              <strong className="block font-extrabold text-green-700">
                Create happiness in their lives.
              </strong>
            </h1>

            <p className="mt-4 max-w-lg sm:text-xl/relaxed text-green-600">
              Spreading Smiles: Unleashing Joy and Crafting Lifelong Happiness
            </p>

            <div className="mt-8 flex flex-wrap text-center">
              <a
                onClick={() => router.push("/create-campaign")}
                className="block w-full rounded bg-green-600 px-12 py-3 text-sm font-medium text-white shadow hover:bg-green-700 focus:outline-none focus:ring active:bg-green-500 sm:w-auto hover:text-white cursor-pointer"
              >
                Get Started
              </a>

              {/* <a
                href="#"
                className="block w-full rounded bg-white px-12 py-3 text-sm font-medium text-green-600 shadow hover:text-green-700 focus:outline-none focus:ring active:text-green-500 sm:w-auto"
              >
                Learn More
              </a> */}
            </div>
          </div>
        </div>
      </section>
    </>
  );
};

export default Banner;
