import React from 'react';
import { Typography, Box, Table, TableBody, TableCell, TableHead, TableRow } from '@mui/material';

import PageContainer from 'src/components/container/PageContainer';
import DashboardCard from 'src/components/shared/DashboardCard';
import { useAllAdminsQuery, useDeleteAdminMutation } from 'src/services/Auth';
import { useNavigate } from 'react-router';
import { useAllFeedBacksQuery } from 'src/services/Feedbacks';

const TypographyPage = () => {
  const { data: dataAllAdmins } = useAllAdminsQuery();
  const { data: dataAllFeedbacks } = useAllFeedBacksQuery();
  console.log('dataAllFeedbacks', dataAllFeedbacks?.data);
  const navigate = useNavigate();

  return (
    <>
      <PageContainer title="Feedbacks | Crowdfunding" description="">
        <DashboardCard title="Feedbacks">
          <Box sx={{ overflow: 'auto', width: { xs: '280px', sm: 'auto' } }}>
            <Table
              aria-label="simple table"
              sx={{
                whiteSpace: 'nowrap',
                mt: 2,
              }}
            >
              <TableHead>
                <TableRow>
                  <TableCell>
                    <Typography variant="subtitle2" fontWeight={600}>
                      SN.
                    </Typography>
                  </TableCell>
                  <TableCell>
                    <Typography variant="subtitle2" fontWeight={600}>
                      Name
                    </Typography>
                  </TableCell>
                  <TableCell>
                    <Typography variant="subtitle2" fontWeight={600}>
                      Email
                    </Typography>
                  </TableCell>
                  <TableCell>
                    <Typography variant="subtitle2" fontWeight={600}>
                      message
                    </Typography>
                  </TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {dataAllFeedbacks?.data?.map(({ attributes, id }, i) => {
                  const userid = id;
                  // console.log('userid', userid);
                  return (
                    <TableRow key={id}>
                      <TableCell>
                        <Typography
                          sx={{
                            fontSize: '15px',
                            fontWeight: '500',
                          }}
                        >
                          {i + 1}
                        </Typography>
                      </TableCell>
                      <TableCell>
                        <Box
                          sx={{
                            display: 'flex',
                            alignItems: 'center',
                          }}
                        >
                          <Box>
                            <Typography variant="subtitle2" fontWeight={600}>
                              {attributes.name}
                            </Typography>
                            {/* <Typography
                            color="textSecondary"
                            sx={{
                              fontSize: '13px',
                            }}
                          >
                            {attributes.email}
                          </Typography> */}
                          </Box>
                        </Box>
                      </TableCell>
                      <TableCell>
                        <Typography color="textSecondary" variant="subtitle2" fontWeight={400}>
                          {attributes.email}
                        </Typography>
                      </TableCell>
                      <TableCell>
                        <Typography variant="h6"> {attributes?.message} </Typography>
                      </TableCell>
                    </TableRow>
                  );
                })}
              </TableBody>
            </Table>
          </Box>
        </DashboardCard>
      </PageContainer>
    </>
  );
};

export default TypographyPage;
