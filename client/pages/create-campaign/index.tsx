import React from "react";
import WebSiteUserLayout from "../../layout";
import { money } from "../../public/assets";
import { MoneyIcon, checkIfImage } from "../../utils";
import { Formik, Field, Form, ErrorMessage } from "formik";
import { useStateContext } from "../../context";
import { ethers } from "ethers";
import { useRouter } from "next/router";

const CreateCampaign = () => {
  const navigate = useRouter();
  const { createCampaign }: any = useStateContext();
  const initialValues = {
    yourName: "",
    campaignTitle: "",
    story: "",
    goal: "",
    endDate: "",
    campaignImage: "",
  };
  const handleSubmit = async (values: any) => {
    // Handle form submission here
    const form = {
      title: values.campaignTitle,
      description: values.story,
      image: values.campaignImage,
      target: values.goal,
      deadline: values.endDate,
    };
    console.log(form);
    checkIfImage(form.image, async (exists: any) => {
      if (exists) {
        await createCampaign({
          ...form,
          target: ethers.utils.parseUnits(form.target, 18),
        });

        navigate.push("/");
      } else {
        alert("Provide valid image URL");
        // await createCampaign({
        //   ...values,
        //   goal: ethers.utils.parseUnits(values.goal, 18),
        //   campaignImage: "",
        // });
        // navigate.push("/");
      }
    });
    // console.log(values);
    // await createCampaign({
    //   ...values,
    //   goal: ethers.utils.parseUnits(values.goal, 18),
    // });
  };
  return (
    <>
      <WebSiteUserLayout>
        <div className="heading-form">
          <h1 className="cc-heading connect-button">Start a Campaign</h1>
          <div className="form-container">
            <div className="flex justify-center items-center h-screen">
              <Formik initialValues={initialValues} onSubmit={handleSubmit}>
                {() => (
                  <Form className="form">
                    <div className="form-group">
                      <label htmlFor="yourName">Your Name *</label>
                      <Field
                        type="text"
                        id="yourName"
                        name="yourName"
                        required
                        autoComplete="off"
                      />
                    </div>
                    <div className="form-group">
                      <label htmlFor="campaignTitle">Campaign Title *</label>
                      <Field
                        type="text"
                        id="campaignTitle"
                        name="campaignTitle"
                        required
                        autoComplete="off"
                      />
                    </div>
                    <div className="form-group">
                      <label htmlFor="campaignTitle">Image *</label>
                      <Field
                        type="text"
                        id="campaignImage"
                        name="campaignImage"
                        required
                        autoComplete="off"
                      />
                    </div>
                    <div className="form-group">
                      <label htmlFor="story">Story *</label>
                      <Field
                        as="textarea"
                        id="story"
                        name="story"
                        rows={10}
                        cols={50}
                        required
                        autoComplete="off"
                      />
                    </div>
                    <div className="w-full flex justify-start items-center p-4 bg-[#61ef3e] h-[120px] rounded-[10px]">
                      <MoneyIcon />
                      <h4>You will get 100% of the raised amount</h4>
                    </div>
                    <div className="form-group">
                      <label htmlFor="goal">Goal *</label>
                      <Field
                        type="text"
                        id="goal"
                        // step={0.1}
                        name="goal"
                        required
                        autoComplete="off"
                      />
                    </div>
                    <div className="form-group">
                      <label htmlFor="endDate">End Date *</label>
                      <Field
                        type="date"
                        id="endDate"
                        name="endDate"
                        required
                        autoComplete="off"
                      />
                    </div>
                    <div className="button-group">
                      <button className="form-submit-btn" type="submit">
                        Submit
                      </button>
                      <button className="form-submit-btn" type="reset">
                        Clear
                      </button>
                    </div>
                  </Form>
                )}
              </Formik>
            </div>
          </div>
        </div>
      </WebSiteUserLayout>
    </>
  );
};

export default CreateCampaign;

// import React, { useState, ChangeEvent, FormEvent } from 'react';
// import { ethers } from 'ethers';
// import { useRouter } from 'next/router';

// import { useStateContext } from '../../context/';
// import { money } from '../../public/assets';
// import {FormField} from '../../components/FormField';
// import { checkIfImage } from '../../utils';
// import { Loader, CustomButton } from './your-component-path'; // Replace with correct paths

// interface FormData {
//   name: string;
//   title: string;
//   description: string;
//   target: string;
//   deadline: string;
//   image: string;
// }

// const CreateCampaign: React.FC = () => {
//   const navigate = useRouter();
//   const [isLoading, setIsLoading] = useState(false);
//   const { createCampaign } = useStateContext();
//   const [form, setForm] = useState<FormData>({
//     name: '',
//     title: '',
//     description: '',
//     target: '',
//     deadline: '',
//     image: '',
//   });

//   const handleFormFieldChange = (fieldName: keyof FormData, e: ChangeEvent<HTMLInputElement | HTMLTextAreaElement>) => {
//     setForm({ ...form, [fieldName]: e.target.value });
//   };

//   const handleSubmit = async (e: FormEvent) => {
//     e.preventDefault();

//     checkIfImage(form.image, async (exists:any) => {
//       if (exists) {
//         setIsLoading(true);
//         await createCampaign({ ...form, target: ethers.utils.parseUnits(form.target, 18) });
//         setIsLoading(false);
//         navigate.push('/');
//       } else {
//         alert('Provide a valid image URL');
//         setForm({ ...form, image: '' });
//       }
//     });
//   };

//   return (
//     <div className="bg-[#1c1c24] flex justify-center items-center flex-col rounded-[10px] sm:p-10 p-4">
//       {isLoading && <Loader />}
//       <div className="flex justify-center items-center p-[16px] sm:min-w-[380px] bg-[#3a3a43] rounded-[10px]">
//         <h1 className="font-epilogue font-bold sm:text-[25px] text-[18px] leading-[38px] text-white">
//           Start a Campaign
//         </h1>
//       </div>

//       <form onSubmit={handleSubmit} className="w-full mt-[65px] flex flex-col gap-[30px]">
//         <div className="flex flex-wrap gap-[40px]">
//           <FormField
//             labelName="Your Name *"
//             placeholder="John Doe"
//             inputType="text"
//             value={form.name}
//             handleChange={(e) => handleFormFieldChange('name', e)}
//           />
//           <FormField
//             labelName="Campaign Title *"
//             placeholder="Write a title"
//             inputType="text"
//             value={form.title}
//             handleChange={(e) => handleFormFieldChange('title', e)}
//           />
//         </div>

//         <FormField
//             labelName="Story *"
//             placeholder="Write your story"
//             isTextArea
//             value={form.description}
//             handleChange={(e) => handleFormFieldChange('description', e)}
//           />

//         <div className="w-full flex justify-start items-center p-4 bg-[#8c6dfd] h-[120px] rounded-[10px]">
//           <img src={money} alt="money" className="w-[40px] h-[40px] object-contain"/>
//           <h4 className="font-epilogue font-bold text-[25px] text-white ml-[20px]">You will get 100% of the raised amount</h4>
//         </div>

//         <div className="flex flex-wrap gap-[40px]">
//           <FormField
//             labelName="Goal *"
//             placeholder="ETH 0.50"
//             inputType="text"
//             value={form.target}
//             handleChange={(e) => handleFormFieldChange('target', e)}
//           />
//           <FormField
//             labelName="End Date *"
//             placeholder="End Date"
//             inputType="date"
//             value={form.deadline}
//             handleChange={(e) => handleFormFieldChange('deadline', e)}
//           />
//         </div>

//         <FormField
//             labelName="Campaign image *"
//             placeholder="Place image URL of your campaign"
//             inputType="url"
//             value={form.image}
//             handleChange={(e) => handleFormFieldChange('image', e)}
//           />

//           <div className="flex justify-center items-center mt-[40px]">
//             <CustomButton
//               btnType="submit"
//               title="Submit new campaign"
//               styles="bg-[#1dc071]"
//             />
//           </div>
//       </form>
//     </div>
//   );
// };

// export default CreateCampaign;
