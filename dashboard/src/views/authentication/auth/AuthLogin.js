// import React from 'react';
// import {
//     Box,
//     Typography,
//     FormGroup,
//     FormControlLabel,
//     Button,
//     Stack,
//     Checkbox
// } from '@mui/material';
// import { Link } from 'react-router-dom';

// import CustomTextField from '../../../components/forms/theme-elements/CustomTextField';
// import { useLoginAdminMutation } from 'src/services/Auth';

// const AuthLogin = ({ title, subtitle, subtext }) => {
//     const[login,{isSuccess,isError,error}]=useLoginAdminMutation()
//     return(
//     <>
//         {title ? (
//             <Typography fontWeight="700" variant="h2" mb={1}>
//                 {title}
//             </Typography>
//         ) : null}

//         {subtext}

//         <Stack>
//             <Box>
//                 <Typography variant="subtitle1"
//                     fontWeight={600} component="label" htmlFor='email' mb="5px">email</Typography>
//                 <CustomTextField id="email" variant="outlined" fullWidth />
//             </Box>
//             <Box mt="25px">
//                 <Typography variant="subtitle1"
//                     fontWeight={600} component="label" htmlFor='password' mb="5px" >Password</Typography>
//                 <CustomTextField id="password" type="password" variant="outlined" fullWidth />
//             </Box>
//             <Stack justifyContent="space-between" direction="row" alignItems="center" my={2}>
//                 <FormGroup>
//                     <FormControlLabel
//                         control={<Checkbox defaultChecked />}
//                         label="Remeber this Device"
//                     />
//                 </FormGroup>
//                 <Typography
//                     component={Link}
//                     to="/"
//                     fontWeight="500"
//                     sx={{
//                         textDecoration: 'none',
//                         color: 'primary.main',
//                     }}
//                 >
//                     Forgot Password ?
//                 </Typography>
//             </Stack>
//         </Stack>
//         <Box>
//             <Button
//                 color="primary"
//                 variant="contained"
//                 size="large"
//                 fullWidth
//                 component={Link}
//                 to="/"
//                 type="submit"
//             >
//                 Sign In
//             </Button>
//         </Box>
//         {subtitle}
//     </>
// )};

// export default AuthLogin;

import React, { useEffect } from 'react';
import { Box, Typography, Button, Stack, Checkbox } from '@mui/material';
import { Link, useNavigate } from 'react-router-dom';
import { useFormik } from 'formik';
import * as Yup from 'yup'; // You might need to install Yup: npm install yup

import CustomTextField from '../../../components/forms/theme-elements/CustomTextField';
import { useLoginAdminMutation } from 'src/services/Auth';

const validationSchema = Yup.object().shape({
  email: Yup.string().required('email is required'),
  password: Yup.string().required('Password is required'),
});

const AuthLogin = ({ title, subtitle, subtext }) => {
  const [login, { data, isSuccess, isError, error }] = useLoginAdminMutation();
  const navigate = useNavigate();
  useEffect(() => {
    console.log(data?.tokens?.refresh?.token);
    if (isSuccess && window) {
      localStorage.setItem('token_crw', data?.tokens?.access?.token);
      localStorage.setItem('rftoken_crw', data?.tokens?.refresh?.token);
      navigate('/dashboard');
    }
  }, [isSuccess, isError, error]);
  const formik = useFormik({
    initialValues: {
      email: '',
      password: '',
      //   rememberDevice: false,
    },
    validationSchema,
    onSubmit: async (values) => {
      try {
        console.log('values,', values);
        await login(values); // Assuming your useLoginAdminMutation handles the login process
        // You can handle successful login here, e.g. redirect or show a success message
      } catch (error) {
        // Handle login error, e.g. display error message
      }
    },
  });

  return (
    <>
      {title && (
        <Typography fontWeight="700" variant="h2" mb={1}>
          {title}
        </Typography>
      )}

      {subtext}

      <form onSubmit={formik.handleSubmit}>
        <Stack>
          <Box>
            <Typography
              variant="subtitle1"
              fontWeight={600}
              component="label"
              htmlFor="email"
              mb="5px"
            >
              Email
            </Typography>
            <CustomTextField
              id="email"
              variant="outlined"
              fullWidth
              error={formik.touched.email && Boolean(formik.errors.email)}
              helperText={formik.touched.email && formik.errors.email}
              {...formik.getFieldProps('email')}
            />
          </Box>
          <Box mt="25px">
            <Typography
              variant="subtitle1"
              fontWeight={600}
              component="label"
              htmlFor="password"
              mb="5px"
            >
              Password
            </Typography>
            <CustomTextField
              id="password"
              type="password"
              variant="outlined"
              fullWidth
              error={formik.touched.password && Boolean(formik.errors.password)}
              helperText={formik.touched.password && formik.errors.password}
              {...formik.getFieldProps('password')}
            />
          </Box>
          {/* <Stack justifyContent="space-between" direction="row" alignItems="center" my={2}>
            <Checkbox
              checked={formik.values.rememberDevice}
              {...formik.getFieldProps('rememberDevice')}
            />
            <Typography
              component={Link}
              to="/"
              fontWeight="500"
              sx={{
                textDecoration: 'none',
                color: 'primary.main',
              }}
            >
              Forgot Password?
            </Typography>
          </Stack> */}
          <br />
          <br />
        </Stack>
        <Box>
          <Button
            color="primary"
            variant="contained"
            size="large"
            fullWidth
            type="submit"
            disabled={formik.isSubmitting}
          >
            Sign In
          </Button>
        </Box>
      </form>
      {subtitle}
    </>
  );
};

export default AuthLogin;
