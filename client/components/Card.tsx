import React, { useEffect, useMemo, useState } from "react";
import { v4 as uuidv4 } from "uuid";
import { useRouter } from "next/router";
import FundCard from "./FundCard";
import { Loader } from "../Icons";

interface Campaign {
  owner: string;
  title: string;
  description: string;
  target: string;
  deadline: string;
  amountCollected: string;
  image: string;
}

interface DisplayCampaignsProps {
  title: string;
  isLoading: boolean;
  campaigns: Campaign[];
}
interface FundCardProps {
  owner: string;
  title: string;
  description: string;
  target: string;
  deadline: string;
  amountCollected: string;
  image: string;
  handleClick: () => void;
}

const DisplayCampaigns: React.FC<DisplayCampaignsProps> = ({
  title,
  isLoading,
  campaigns,
}) => {
  const navigate = useRouter();

  // const handleNavigate = (campaign: Campaign) => {
  //   navigate.push(`/campaign-details`);
  // };
  const handleNavigate = (campaign: Campaign) => {
    // const stateObject = { key: 'value', anotherKey: 'anotherValue' };
    navigate.push({
      pathname: "/campaign-details",
      query: campaign as any,
    });
  };
  console.log(campaigns);
  const [searchTerm, setSearchTerm] = useState(""); // Replace with the search term you're looking for
  const [dataCamp, setDataCamp] = useState(campaigns);
  const lowerCaseSearchTerm = searchTerm.toLowerCase(); // Convert search term to lowercase

  const filteredArray = useMemo(() => {
    if (searchTerm) {
      return campaigns?.filter((item) => {
        const descriptionMatch = item.description
          .toLowerCase()
          .includes(lowerCaseSearchTerm);
        const titleMatch = item.title
          .toLowerCase()
          .includes(lowerCaseSearchTerm);
        return descriptionMatch || titleMatch;
      });
    } else {
      return campaigns;
    }
  }, [campaigns, searchTerm, lowerCaseSearchTerm]);
  const handleChange = (e: any) => {
    e.preventDefault();
    setSearchTerm(e.target.value);
  };
  useEffect(() => {
    if (searchTerm) {
      setDataCamp(filteredArray);
    } else {
      setDataCamp(campaigns);
    }
  }, [filteredArray, campaigns, searchTerm]);

  return (
    <div className="m-[41px] rounded-[50px]">
      <h1 className="font-epilogue font-semibold text-[18px] text-white text-left">
        {title} ({dataCamp.length})
      </h1>
      <div className="formFilter">
        <span className="input-span">
          <label htmlFor="email" className="label">
            Filter By Title or Description:
          </label>
          <input
            type="text"
            value={searchTerm}
            name="email"
            onChange={(e) => handleChange(e)}
            id="email"
            autoComplete="off"
          />
        </span>
      </div>
      <div className="flex flex-wrap mt-[20px] gap-[26px]">
        {isLoading && <Loader />}

        {!isLoading &&
          (dataCamp.length === 0 || filteredArray.length === 0) && (
            <p className="font-epilogue font-semibold text-[14px] leading-[30px] text-[#818183]">
              {searchTerm
                ? "Not any related data to this text"
                : "You have not created any campaigns yet"}
            </p>
          )}

        {!isLoading &&
          dataCamp?.length > 0 &&
          dataCamp?.map((campaign) => (
            <FundCard
              key={uuidv4()}
              owner={campaign.owner}
              title={campaign.title}
              description={campaign.description}
              target={campaign.target}
              deadline={campaign.deadline}
              amountCollected={campaign.amountCollected}
              image={campaign.image}
              handleClick={() => handleNavigate(campaign)}
            />
          ))}
      </div>
    </div>
  );
};

export default DisplayCampaigns;
