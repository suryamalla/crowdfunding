import { NextPage } from "next";
import { useEffect, useState } from "react";
import WebSiteUserLayout from "../layout";
import Card from "../components/Card";
import { useStateContext } from "../context";
import Banner from "../components/Banner";
import { daysLeft } from "../utils";

const Home: NextPage = () => {
  const [isLoading, setIsLoading] = useState<boolean>(false);
  const [campaigns, setCampaigns] = useState<any[]>([]);
  const { address, contract, getCampaigns } = useStateContext();

  const fetchCampaigns = async () => {
    setIsLoading(true);
    const data = await getCampaigns();
    setCampaigns(data);
    setIsLoading(false);
  };

  useEffect(() => {
    if (contract) fetchCampaigns();
  }, [address, contract]);
  const [isLatestActive, SetLatestActive] = useState(true);
  const [isOldestActive, SetOldestActive] = useState(false);
  const [filteredCampaigns, setFilteredCampaigns] = useState<any[]>([]);
  useEffect(() => {
    if (isLatestActive && campaigns) {
      // Sort campaigns by deadline in ascending order (oldest first)
      const sortedCampaigns = campaigns
        .slice()
        .sort((a, b) => a.deadline - b.deadline);

      // Get the latest 5 campaigns
      const latestCampaigns = sortedCampaigns.slice(-5);

      // Get the oldest campaigns
      const oldestCampaigns = sortedCampaigns.slice(0, 5);
      setFilteredCampaigns(latestCampaigns);
      console.log("Latest Campaigns:", latestCampaigns);
      console.log("Oldest Campaigns:", oldestCampaigns);
    }
    if (isOldestActive && campaigns) {
      // Sort campaigns by deadline in ascending order (oldest first)
      const sortedCampaigns = campaigns
        .filter((campaign) => Number(daysLeft(campaign.deadline)) >= 0)
        .slice()
        .sort((a, b) => a.deadline - b.deadline);

      // Get the latest 5 campaigns
      const latestCampaigns = sortedCampaigns
        .slice(-5)
        .filter((campaign) => campaign.deadline >= 0);

      // Get the oldest campaigns
      const oldestCampaigns = sortedCampaigns
        .slice(0, 5)
        .filter((campaign) => campaign.deadline >= 0);
      setFilteredCampaigns(oldestCampaigns);
      console.log("Latest Campaigns:", latestCampaigns);
      console.log("Oldest Campaigns:", oldestCampaigns);
    }
  }, [isLatestActive, isOldestActive, campaigns]);

  return (
    <>
      <WebSiteUserLayout>
        <Banner />
        <div className="toggle-button">
          <button
            className={isLatestActive ? "button-active" : "button-latest"}
            onClick={() => {
              return SetOldestActive(false), SetLatestActive(true);
            }}
          >
            Latest Campaigns
          </button>
          <button
            className={isOldestActive ? "button-active" : "button-oldest"}
            onClick={() => {
              return SetOldestActive(true), SetLatestActive(false);
            }}
          >
            Oldest Campaigns
          </button>
        </div>
        <Card
          title={isOldestActive ? "Oldest Campaigns" : "Latest Campaigns"}
          isLoading={isLoading}
          campaigns={filteredCampaigns}
        />
      </WebSiteUserLayout>
    </>
  );
};

export default Home;
