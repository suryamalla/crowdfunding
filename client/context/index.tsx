import React, { createContext, useContext, ReactNode } from "react";
import {
  useAddress,
  useContract,
  useMetamask,
  useContractWrite,
} from "@thirdweb-dev/react";
import { ethers } from "ethers";
import { EditionMetadataWithOwnerOutputSchema } from "@thirdweb-dev/sdk";

interface Campaign {
  owner: string;
  title: string;
  description: string;
  target: string;
  deadline: number;
  amountCollected: string;
  image: string;
  pId: number;
}

interface StateContextValue {
  address: string | null;
  contract: any; // Adjust the type of contract according to your contract structure
  connect: () => void;
  createCampaign: (form: any) => Promise<void>;
  getCampaigns: () => Promise<Campaign[]>;
  getUserCampaigns: () => Promise<Campaign[]>;
  donate: (pId: number, amount: string) => Promise<any>;
  getDonatiors: (pId: number) => Promise<any[]>;
}

const StateContext = createContext<StateContextValue | undefined>(undefined);

interface StateContextProviderProps {
  children: ReactNode;
}
interface FormData {
  address: string;
  image: string;
  title: string;
  description: string;
  deadline: number;
  target: string;
}
export const StateContextProvider: React.FC<StateContextProviderProps> = ({
  children,
}) => {
  // const { contract } = useContract(
  //   "0x81Df522e263C621f019700b22949B7656Cf55090"
  // );
  const contractHook = useContract(
    "0x81Df522e263C621f019700b22949B7656Cf55090"
  );

  const contract = contractHook.contract; // Extract contract from the hook result

  const { mutateAsync: createCampaign } = useContractWrite(
    contract,
    "createCampaign"
  );

  const address: any = useAddress();
  const connect = useMetamask();

  const publishCampaign = async (form: FormData): Promise<void> => {
    if (!contract) return;
    try {
      const data = await createCampaign({
        args: [
          address as string, // owner
          form.title, // title
          form.description, // description
          form.target,
          new Date(form.deadline).getTime(), // deadline,
          form.image,
        ],
      });

      console.log("contract call success", data);
    } catch (error) {
      console.log("contract call failure", error);
    }
  };
  const getCampaigns = async (): Promise<Campaign[]> => {
    if (!contract) return [];
    const campaigns = await contract.call("getCampaigns");

    const parsedCampaings: Campaign[] = campaigns.map(
      (campaign: any, i: number) => ({
        owner: campaign.owner,
        title: campaign.title,
        description: campaign.description,
        target: ethers.utils.formatEther(campaign.target.toString()),
        deadline: campaign.deadline.toNumber(),
        amountCollected: ethers.utils.formatEther(
          campaign.amountCollected.toString()
        ),
        image: campaign.image,
        pId: i,
      })
    );

    return parsedCampaings;
  };
  const getUserCampaigns = async (): Promise<Campaign[]> => {
    const allCampaigns = await getCampaigns();

    const filteredCampaigns = allCampaigns.filter(
      (campaign) => campaign.owner === address
    );

    return filteredCampaigns;
  };
  // console.log("getUserCampaigns", getUserCampaigns());
  const donate = async (pId: number, amount: string): Promise<any> => {
    if (!contract) return [];
    const data = await contract.call("donateToCampaign", [pId], {
      value: ethers.utils.parseEther(amount),
    });

    return data;
  };
  const getDonatiors = async (pId: number): Promise<any[]> => {
    if (!contract) return [];
    const donations = await contract.call("getDonatiors", [pId]);
    const numberOfDonations = donations[0].length;

    const parsedDonations = [];

    for (let i = 0; i < numberOfDonations; i++) {
      parsedDonations.push({
        donator: donations[0][i],
        donation: ethers.utils.formatEther(donations[1][i].toString()),
      });
    }

    return parsedDonations;
  };
  return (
    <StateContext.Provider
      value={{
        address,
        contract,
        connect,
        createCampaign: publishCampaign,
        getCampaigns,
        getUserCampaigns,
        donate,
        getDonatiors,
      }}
    >
      {children}
    </StateContext.Provider>
  );
};

export const useStateContext = (): StateContextValue => {
  const context = useContext(StateContext);
  if (!context) {
    throw new Error(
      "useStateContext must be used within a StateContextProvider"
    );
  }
  return context;
};
