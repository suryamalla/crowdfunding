import React from 'react';
import {
  Typography,
  Box,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
  Chip,
} from '@mui/material';

import PageContainer from 'src/components/container/PageContainer';
import DashboardCard from 'src/components/shared/DashboardCard';
import { useAllAdminsQuery, useDeleteAdminMutation } from 'src/services/Auth';
import { useNavigate } from 'react-router';

const UserLists = () => {
  const { data: dataAllAdmins } = useAllAdminsQuery();
  const [deleteF, { data }] = useDeleteAdminMutation();

  const navigate = useNavigate();
  const handleEdit = (userid) => {
    console.log('usrid', userid);
    const userIdAsString = String(userid);
    console.log('userIdAsString', userIdAsString);
    navigate(`/update-user/${userid}`);
  };
  const handleDelete = (userid) => {
    deleteF(userid);
  };
  return (
    <>
      <PageContainer title="User List | Crowdfunding" description="">
        <DashboardCard title="User List">
          <Box sx={{ overflow: 'auto', width: { xs: '280px', sm: 'auto' } }}>
            <Table
              aria-label="simple table"
              sx={{
                whiteSpace: 'nowrap',
                mt: 2,
              }}
            >
              <TableHead>
                <TableRow>
                  <TableCell>
                    <Typography variant="subtitle2" fontWeight={600}>
                      SN.
                    </Typography>
                  </TableCell>
                  <TableCell>
                    <Typography variant="subtitle2" fontWeight={600}>
                      Name
                    </Typography>
                  </TableCell>
                  <TableCell>
                    <Typography variant="subtitle2" fontWeight={600}>
                      Email
                    </Typography>
                  </TableCell>
                  <TableCell>
                    <Typography variant="subtitle2" fontWeight={600}>
                      Role
                    </Typography>
                  </TableCell>
                  <TableCell>
                    <Typography variant="subtitle2" fontWeight={600}>
                      Action
                    </Typography>
                  </TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {dataAllAdmins?.results?.map((product, i) => {
                  const userid = product?.id;
                  // console.log('userid', userid);
                  return (
                    <TableRow key={product?.id}>
                      <TableCell>
                        <Typography
                          sx={{
                            fontSize: '15px',
                            fontWeight: '500',
                          }}
                        >
                          {i + 1}
                        </Typography>
                      </TableCell>
                      <TableCell>
                        <Box
                          sx={{
                            display: 'flex',
                            alignItems: 'center',
                          }}
                        >
                          <Box>
                            <Typography variant="subtitle2" fontWeight={600}>
                              {product.name}
                            </Typography>
                            {/* <Typography
                            color="textSecondary"
                            sx={{
                              fontSize: '13px',
                            }}
                          >
                            {product.email}
                          </Typography> */}
                          </Box>
                        </Box>
                      </TableCell>
                      <TableCell>
                        <Typography color="textSecondary" variant="subtitle2" fontWeight={400}>
                          {product.email}
                        </Typography>
                      </TableCell>
                      <TableCell>
                        <Typography variant="h6"> {product?.role} </Typography>
                      </TableCell>

                      <TableCell align="right">
                        <Chip
                          onClick={() => {
                            handleEdit(userid);
                          }}
                          sx={{
                            px: '4px',
                            backgroundColor: 'green',
                            color: '#fff',
                          }}
                          size="small"
                          label="Edit"
                        ></Chip>
                        &nbsp;&nbsp;&nbsp;
                        <Chip
                          onClick={() => handleDelete(userid)}
                          sx={{
                            px: '4px',
                            backgroundColor: 'red',
                            color: '#fff',
                          }}
                          size="small"
                          label="Delete"
                        ></Chip>
                      </TableCell>
                    </TableRow>
                  );
                })}
              </TableBody>
            </Table>
          </Box>
        </DashboardCard>
      </PageContainer>
    </>
  );
};

export default UserLists;
